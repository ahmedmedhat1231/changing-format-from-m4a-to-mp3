const express = require('express')
const ffmpeg = require('fluent-ffmpeg')
const app = express()
const fs = require('fs')


const read = ()=>{ 
  
  return new Promise((resolve, reject) =>{
    fs.readdir(__dirname+'/Downloads', (err, files) => {
    if (err)
      console.log(err);
    else {
      console.log("\nCurrent directory filenames:");
      console.log(JSON.stringify(files));
      files.forEach(file => {
        console.log('ok','Downloads/'+file);

        ffmpeg.setFfmpegPath('C:/ffmpeg/bin/ffmpeg.exe')

const outputFileName = file.replace('m4a','mp3')

        // converting m4a to mp3
    ffmpeg('Downloads/'+file).toFormat('mp3')
    .on('end', function() 
    {
       console.log('Done');
      resolve('Done');
      })
    .on('error', function(err) 
    { console.log(err.message);
    reject(err.message);
    }).save('Downloads/'+outputFileName)
      })
    }
  })
  });
}



app.get('/', (req, res) => {
    res.send(read())
})


app.listen(2000)
console.log('listening to port 2000 ...');


  